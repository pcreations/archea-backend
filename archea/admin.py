from django.contrib import admin
from .models import Color, Link, IconButton


@admin.register(Color)
class ColorAdmin(admin.ModelAdmin):
    pass


@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    pass


@admin.register(IconButton)
class IconButtonAdmin(admin.ModelAdmin):
    pass
