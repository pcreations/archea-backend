from rest_framework import routers, serializers, viewsets
from django.urls import path, include
from archea.models import Color, Link, IconButton

# Serializers define the API representation.


class ColorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Color
        fields = ['name', 'value']


class LinkSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Link
        fields = ['title', 'is_portrait', 'url', 'color', 'image']


class IconButtonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = IconButton
        fields = ['icon_type', 'icon_name', 'url', 'is_portrait']

# ViewSets define the view behavior.


class ColorViewSet(viewsets.ModelViewSet):
    queryset = Color.objects.all()
    serializer_class = ColorSerializer


class LinkViewSet(viewsets.ModelViewSet):
    queryset = Link.objects.all()
    serializer_class = LinkSerializer


class IconButtonViewSet(viewsets.ModelViewSet):
    queryset = IconButton.objects.all()
    serializer_class = IconButtonSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'colors', ColorViewSet)
router.register(r'links', LinkViewSet)
router.register(r'buttons', IconButtonViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
