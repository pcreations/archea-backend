# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-12-13 21:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=7)),
            ],
        ),
        migrations.CreateModel(
            name='IconButton',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('icon_type', models.CharField(max_length=15)),
                ('icon_name', models.CharField(max_length=15)),
                ('url', models.CharField(max_length=255)),
                ('is_portrait', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('is_portrait', models.BooleanField(default=True)),
                ('url', models.CharField(max_length=255)),
                ('image', models.ImageField(max_length=255, upload_to='images/')),
                ('color', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='archea.Color')),
            ],
        ),
    ]
