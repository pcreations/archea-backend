from __future__ import unicode_literals

from django.db import models


class Color(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=7)


class Link(models.Model):
    title = models.CharField(max_length=255)
    is_portrait = models.BooleanField(default=True)
    url = models.CharField(max_length=255)
    color = models.ForeignKey(Color, on_delete=models.DO_NOTHING)
    image = models.ImageField(max_length=255, upload_to='images/')


class IconButton(models.Model):
    icon_type = models.CharField(max_length=15)
    icon_name = models.CharField(max_length=15)
    url = models.CharField(max_length=255)
    is_portrait = models.BooleanField(default=True)
